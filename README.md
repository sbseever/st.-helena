# README #

A Night at Saint Helena is a horror game created for a class project. The point of the game is to collect the 4 pages scattered around the inside of the school,
and get back to the blinking locker to input the correct combination outlined on the pages.

To play the game, simply unpack the proper zip (StHelenaWindows, for windows, and A Night at St. Helena for macOS)and launch the executable. 

The controls:
WASD - movement
Shift - sprint
Space - Jump
Z - Toggle flashlight
F - Interact/Pick up pages
X - view pages collected

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact